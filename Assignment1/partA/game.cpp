#include "game.h"

//Constructor for the game, retrieve settings and start the game
Game::Game()
{
    GetSettings();
    StartGame();
}

//Set the game's settings: # players, and starting pile size. Then initialize the board
void Game::GetSettings(){
    std::string userInput;

    std::cout<<"Welcome to The Game of Nim!\n1 or 2 players? ";
    getline(std::cin, userInput);
    num_players_ = std::stoi(userInput);

    std::cout<<"Player 1 - Enter Name: ";
    getline(std::cin, player_one_);

    if(num_players_ == 2){
        std::cout<<"Player 2 - Enter Name: ";
        getline(std::cin, player_two_);
    }else{
        player_two_ = "CPU";
        std::cout<<"Max depth for the CPU to calculate?";
        getline(std::cin, userInput);
        max_depth_ = std::stoi(userInput);
    }

    std::cout<<"What is the starting pile size? ";
    getline(std::cin, userInput);
    starting_pile_size_ = std::stoi(userInput);

    board_ = new GrundyBoard(starting_pile_size_);
    current_player_ = rand()%2+1;
    return;
}

//Main game loop.
void Game::StartGame(){
    std::cout<<"\nGame Started, good luck!\nMake all moves in the following format (without brackets):\n[pileToShrink numSticksToTakeAway]\n";

    //Loop until the board has reached an end state
    while(!GameOver(board_)){
        //Display the board to the player
        board_->PrintBoard();
        //Get this player's turn
        TakeTurn();
        //Swap which player gets to move
        current_player_ = SWAP_PLAYER(current_player_);
    }

    //Swap current player a final time for correct win message
    current_player_ = SWAP_PLAYER(current_player_);
    std::cout<<"\nGame Over!";
    board_->PrintBoard();

    //Display win message
    std::cout<<GetPlayerName()<<" wins!\n";

	int test;
	std::cin >> test;

    delete(board_);
}

//Let the current player take their turn
void Game::TakeTurn(){
    //Variables to store the player's decisions
    int pileNum = 0, numSticks = 0;

    //If this is a single player game and it is the CPU's turn
    if(num_players_ == 1 && current_player_ == 2){
        //Uncommend to run with tree
            DecisionTree(board_, max_depth_, true);
    }else{

        //do-while, we always want to get a move before we check its validity
        do{
            std::cout<<GetPlayerName()<<", enter your move: ";
            std::cin>>pileNum>>numSticks;

            //Loop until a valid move is entered
        }while(!board_->MakeMove(pileNum, numSticks));

    }
}

//Checks to see if the game is over
bool Game::GameOver(GrundyBoard *currBoard){
    //Get the head node in the board's list of piles
    Pile *tempPile = currBoard->GetPiles();

    //While we're pointing at valid pile
    while(tempPile){
        //If there exists any pile of size > 2, the game is not over
        if(tempPile->GetSticks() > 2){
            return false;
        }
        //Advance our pointer to the next pile
        tempPile = tempPile->GetNextPile();
    }
    //No piles > 2 exist, the game is over
    return true;
}

//Checks to see which is the current player, return the appropriate name
std::string Game::GetPlayerName(){
    return ((current_player_ == 1) ? (player_one_):(player_two_));
}

//CPU is the maximizing player
int Game::DecisionTree(GrundyBoard *currBoard, int depth, bool CPU){
    
    //TODO: Write your AI code here

    GrundyBoard* cpBoard = currBoard->CopyBoard();
    int possibleMoves = CalulatePossibleMoves(cpBoard);
    int** moveList = GetLegalMoves(cpBoard, possibleMoves);
    int value;
    
    //if depth==0 or node is terminal - return value of node
    if(depth==0 || GameOver(cpBoard)){
        //evaluation function
        if(CPU){
            value = 10;
        }
        else{
            value = -10;
        }

        return value;
    }
    
    //maximizing player
    if(CPU){
        //std::cout<<"maximizing player\n";
        value = -900;
        int higherM = 0;
        for(int m=0; m<possibleMoves; m++){
            //std::cout<<"m:"<<m<<"\n";
            //std::cout<<"move:"<<moveList[m][0]<<" "<<moveList[m][1]<<"\n";
            //make move
            cpBoard->MakeMove(moveList[m][0], moveList[m][1]);
            int oldValue = value;
            value = std::max(value, DecisionTree(cpBoard, depth-1, false));
            if(value>oldValue){
                higherM = m;
            }
        }
        //want to make the move with the highest possible score
        currBoard->MakeMove(moveList[higherM][0], moveList[higherM][1]);
        return value;
    }
    //minimizing player
    else{
        //std::cout<<"minimizing player\n";
        value = 900;
        int lowerM = 0;
        for(int m=0; m<possibleMoves; m++){
            //std::cout<<"m:"<<m<<"\n";
            //std::cout<<"move:"<<moveList[m][0]<<" "<<moveList[m][1]<<"\n";
            //make move
            cpBoard->MakeMove(moveList[m][0], moveList[m][1]);
            int oldValue = value;
            value = std::min(value, DecisionTree(cpBoard, depth-1, true));
            if(value<oldValue){
                lowerM = m;
            }
        }

        //want to make the move with the lowest possible score
        currBoard->MakeMove(moveList[lowerM][0], moveList[lowerM][1]);
        return value;
    }
    
	//Dummy return
	return 0;
}

int** Game::GetLegalMoves(GrundyBoard *currBoard, int numLegalMoves){
    //List to store the moves we calculate
    int **moveList = new int*[numLegalMoves];

    //Loop the list and create an array for each pointer
    for(int i = 0; i < numLegalMoves; i++){
        //pos 1 pile, pos 2 sticks to take away
        moveList[i] = new int[2];
    }

    //Get the head pointer of our board's piles
    Pile *currPile = currBoard->GetPiles();

    //Counter to calculate which move we are making
    //currNumSticks to avoid repeated function calls
    //difference to avoid repeatedly performing the same arithmetic
    int counter = 0, currNumSticks, difference = 0;

    //Loop all the piles in this board
    for(int i = 0; i < currBoard->GetNumPiles(); i++){
        //ADD: some way of keeping track of what pile on
        
        //Get the current number of sticks in this pile
        currNumSticks = currPile->GetSticks();

        //TODO: Calculate the moves we could make from this pile [Done]

        //even number of sticks
        if(currNumSticks%2==0 && currNumSticks>2){
            //do for loop for first half-1
            for(int j=1; j<=(currNumSticks/2)-1; j++){
                moveList[counter][0] = i+1;
                moveList[counter][1] = j;
                counter++;
            }
        }
        //odd number of sticks
        else if(currNumSticks%2!=0 && currNumSticks>2){
            //do for loop for first half
            for(int j=1; j<=(currNumSticks/2); j++){
                moveList[counter][0] = i+1;
                moveList[counter][1] = j;
                counter++;
            }

        }

        //Advance our pile pointer
        currPile = currPile->GetNextPile();
    }

    //Return our calculated list
    return moveList;
}

int Game::CalulatePossibleMoves(GrundyBoard *currBoard){
    //Set our pointer to the head of the piles pointer for this board
    Pile *currPile = currBoard->GetPiles();

    //Counter to keep track of how many moves are legal
    //currNumSticks to avoid repeated function calls
    int counter = 0, currNumSticks;

    //TODO: Calculate the number of moves we can make [DONE]
    for(int i = 0; i < currBoard->GetNumPiles(); i++){
        currNumSticks = currPile->GetSticks();
        //std::cout<<"Current number of sticks: "<<currNumSticks<<"\n";
        if(currNumSticks<3){
            //cannot make any moves
        }
        //even number of sticks
        else if(currNumSticks%2==0){
            counter += (currNumSticks/2)-1;
        }
        //odd number of sticks
        else{
            counter += (currNumSticks/2);
        } 
        //std::cout<<"counter: "<<counter<<"\n";       
        currPile = currPile->GetNextPile();
    }   
   
    //std::cout<<"counter total: "<<counter<<"\n";
    //Return the number of moves we can make
    return counter;
}
