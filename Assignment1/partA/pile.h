#ifndef PILE_H
#define PILE_H


class Pile
{
public:
    Pile();
    ~Pile();    
    Pile(int sticks);
    void AddPile(int sticks);
    Pile *GetPile(int pileNum);
    int GetSticks();
    void SetSticks(int sticks);

    void SubtractSticks(int numSticks);
    Pile *GetNextPile();
private:
    int sticks_in_pile_;
    Pile *next_pile_;
};

#endif // PILE_H
