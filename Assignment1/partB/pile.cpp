#include "pile.h"

//Default constructor, 0 sticks and no other piles
Pile::Pile()
{
    sticks_in_pile_ = 0;
    next_pile_ = nullptr;
}

//Constructor that creates a pile with n number of sticks
Pile::Pile(int sticks){
    sticks_in_pile_ = sticks;
    next_pile_ = nullptr;
}

Pile::~Pile(){
    delete(next_pile_);
}

//Add a new pile to the end of the list
void Pile::AddPile(int sticks){
    //Start at the head of the list
    Pile *tempPile = this;

    //While we haven't made it to the end of the list
    while(tempPile->next_pile_ != nullptr){
        //Advance our pointer
        tempPile = tempPile->next_pile_;
    }

    //We're at the end, create a new Pile
    tempPile->next_pile_ = new Pile(sticks);
}

//Accepts an index and returns that pile from the list
Pile* Pile::GetPile(int pileNum){
    //Start our pointer at the head of the list
    Pile *tempPile = this;
    //Start our current index at 1
    int currPileNum = 1;

    //While we have not iterated pileNum times
    while(currPileNum != pileNum){
        //Advance our pointer
        tempPile = tempPile->next_pile_;
        //Increment out iterator
        currPileNum++;
    }

    //Return the pile we stopped on
    return tempPile;
}

//Returns this pile's number of sticks
int Pile::GetSticks(){
    return sticks_in_pile_;
}

//Adjusts this pile's number of sticks due to a valid move
void Pile::SubtractSticks(int numSticks){
    sticks_in_pile_ -= numSticks;
}

//Returns the next pile in the list
Pile* Pile::GetNextPile(){
    return next_pile_;
}

void Pile::SetSticks(int sticks){
    sticks_in_pile_ = sticks;
}
