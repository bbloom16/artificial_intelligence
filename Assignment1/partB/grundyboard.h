#ifndef GRUNDYBOARD_H
#define GRUNDYBOARD_H

#include <iostream>
#include "pile.h"
class GrundyBoard
{
public:
    GrundyBoard();
    GrundyBoard(int piles);
    ~GrundyBoard();
    bool MakeMove(int pileNum, int numSticks);
    void PrintBoard();
    Pile* GetPiles();
    int GetNumPiles();
    int GetStartingPileSize();
    GrundyBoard* CopyBoard();
private:
    int starting_pile_size_;
    int number_of_piles_;
    Pile *pile_list_root_;
};

#endif // GRUNDYBOARD_H
