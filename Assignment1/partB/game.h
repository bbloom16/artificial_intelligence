#ifndef GAME_H
#define GAME_H

#include "grundyboard.h"
#include <time.h>
#include <stdlib.h>
#include <string>

#define SWAP_PLAYER(a) ((a) == (1) ? (2):(1))

class Game
{
public:
    Game();
    void GetSettings();
private:
    std::string player_one_;
    std::string player_two_;
    int num_players_;
    int starting_pile_size_;
    GrundyBoard *board_;
    int current_player_;
    int max_depth_;

    void StartGame();
    void MainGameLoop();
    void TakeTurn();
    std::string GetPlayerName();
    bool GameOver(GrundyBoard *currBoard);
    int CalulatePossibleMoves(GrundyBoard *currBoard);
    int DecisionTree(GrundyBoard *currBoard, int depth, int alph, int beta, bool CPU);
    int** GetLegalMoves(GrundyBoard *currBoard, int numLegalMoves);

};

#endif // GAME_H
