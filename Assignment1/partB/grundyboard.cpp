#include "grundyboard.h"

//Constructor that sets the initial board as 1 pile of n sticks
GrundyBoard::GrundyBoard(int sticks){
    starting_pile_size_ = sticks;
    pile_list_root_ = new Pile(starting_pile_size_);
    number_of_piles_ = 1;
}

GrundyBoard::~GrundyBoard(){
	delete(pile_list_root_);
}

//Modifies the selected pile by the amount of sticks to be removed
bool GrundyBoard::MakeMove(int pileNum, int numSticks){
    //Retrieve the pile we wish to modify
    Pile *currPile = pile_list_root_->GetPile(pileNum);
    //Retrieve the number of sticks in this pile
    int currPilesNumSticks = currPile->GetSticks();

    //If we try to remove 0 sticks, OR
    //The number of sticks in this pile is less than the number we are trying to remove, OR
    //Subtracting the sticks gives us 2 equal piles, OR
    //We make a pile of < 1 sticks
    //Return false, this move was invalid
    if(!numSticks || currPilesNumSticks < numSticks || currPilesNumSticks - numSticks == numSticks || currPilesNumSticks - numSticks < 1){
        //std::cout<<"Invalid move!\n";
        return false;
    }else{
        //We have made a valid move, so subtract the selected number of sticks from this pile
        currPile->SubtractSticks(numSticks);
        //Create a new pile with the leftover sticks
        pile_list_root_->AddPile(numSticks);
        //Increment the number of piles we have
        number_of_piles_++;
        //Return true, this was a valid move
        return true;
    }
}

//Print the list of piles in this board
void GrundyBoard::PrintBoard(){
    Pile *currPile = pile_list_root_;

    std::cout<<"\n\nCurrent Piles: ";

    //While our pointer is valid
    while(currPile){
        //Print the number of sticks in this pile
        std::cout<<currPile->GetSticks()<<" ";
        //Advance our pointer to the next pile
        currPile = currPile->GetNextPile();
    }
    std::cout<<"\n";
}

//Return the number of piles in this board
Pile* GrundyBoard::GetPiles(){
    return pile_list_root_;
}

int GrundyBoard::GetNumPiles(){
    return number_of_piles_;
}

int GrundyBoard::GetStartingPileSize(){
    return starting_pile_size_;
}

GrundyBoard* GrundyBoard::CopyBoard(){
    GrundyBoard *newBoard = new GrundyBoard(starting_pile_size_);

    newBoard->number_of_piles_ = number_of_piles_;

    newBoard->pile_list_root_->SetSticks(pile_list_root_->GetSticks());

    for(int i = 2; i <= number_of_piles_; i++){
        newBoard->pile_list_root_->AddPile(pile_list_root_->GetPile(i)->GetSticks());
    }

    return newBoard;
}
