#include "node.h"
#include <vector>

Node::Node(){
    Node* parent;
    int weight=999;
    bool canUse;
    bool visited;
    vector<Node*> neigh;
    vector<int> neighWeight;
    int xPos;
    int yPos;
}//default constructor

void Node::printNeigh(){
    int numNeigh = 0;
    for(auto x : neigh){
        std::cout<<x->xPos<<","<<x->yPos<<"; ";
        numNeigh++;
    }

    std::cout<<numNeigh<<std::endl;
}