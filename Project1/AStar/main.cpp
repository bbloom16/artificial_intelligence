#include "node.cpp"
#include "CImg.h"
#include <iostream>
#include <vector>
#include <queue>
#include <stack>
#include <chrono>
#include <ctime>

using namespace std::chrono;

cimg_library::CImg<unsigned char> image("divideandconquer.jpg"), visu(500,400,3,0);
cimg_library::CImgDisplay mainDisplay(image, "A*");
const unsigned char redColor[] = {255, 0, 0};//red line
int w, h;//width and height of the image
Node* matrix[700][700];//matrix of image based on what pixels can be acessed (has to be hardcoded here)

struct color{//used for passing rgb values
	int red;
	int green;
	int blue;
};

struct coord{//used for passing coordinates (x,y)
	int x;
	int y;
};

struct compareTo{
	bool operator()(Node* nodeA, Node* nodeB){
		return nodeA->weight > nodeB->weight;
	}
};

bool canPass(coord cordPassed);

color getColor(coord cordPassed){
	int x = cordPassed.x;
	int y = cordPassed.y;
	
	int r = (int)image(x,y,0,0);
	int g = (int)image(x,y,0,1);
	int b = (int)image(x,y,0,2);
	
	color c;
	c.red = r;
	c.green = g;
	c.blue = b;

	/*
	std::cout<<"R: "<<c.red<<std::endl;
	std::cout<<"G: "<<c.green<<std::endl;
	std::cout<<"B: "<<c.blue<<std::endl;
	*/

	return c;
}//getColor

bool canPass(coord cordPassed){//method to determine if color at a specific point can be used, returns true if can, false if can't use
	int x = cordPassed.x;
	int y = cordPassed.y;

	color c = getColor(cordPassed);
	
	if(c.red<100 && c.blue<100){
		return false;
	}
	return true;
}//end method canPass

void initMatrix(){
    for(int i=0; i<w; i++){
        for(int j=0; j<h; j++){
            Node* tempNode = new Node();
			coord center;
			center.x = i;
			center.y = j;

			tempNode->canUse = canPass(center);
			tempNode->xPos = center.x;
			tempNode->yPos = center.y;
			tempNode->weight = 999;
			
			matrix[i][j] = tempNode;
			//std::cout<<"tempNode: "<<tempNode->xPos<<","<<tempNode->yPos<<std::endl;
        }
    }

	for(int i=0; i<w; i++){
        for(int j=0; j<h; j++){
			
			Node* tempNode = matrix[i][j];
			coord center;
			center.x = i;
			center.y = j;
			//overflow checkers
			//true if can use
			bool xLess = true;
			bool xMore = true;
			bool yLess = true;
			bool yMore = true;
			
			if(center.x-1<0){
				xLess = false;
			}
			if(center.x+2>w){
				xMore = false;
			}
			if(center.y-1<0){
				yLess = false;
			}
			if(center.y+2>h){
				yMore = false;
			}
			//std::cout<<xLess<<xMore<<yLess<<yMore<<std::endl;
			//init pointers to all neighbors
			if(xLess && yLess){//topLeft
				Node* topLeft = matrix[center.x-1][center.y-1];
				tempNode->neigh.push_back(topLeft);
			}
			if(xLess){//topMid
				Node* topMid = matrix[center.x-1][center.y];
				tempNode->neigh.push_back(topMid);
			}
			if(xLess && yMore){//topRight
				Node* topRight = matrix[center.x-1][center.y+1];
				tempNode->neigh.push_back(topRight);
			}
			if(yLess){//midLeft
				Node* midLeft = matrix[center.x][center.y-1];
				tempNode->neigh.push_back(midLeft);
			}
			if(yMore){//midRight
				Node* midRight = matrix[center.x][center.y+1];
				tempNode->neigh.push_back(midRight);
			}
			if(xMore && yLess){//btmLeft
				Node* btmLeft = matrix[center.x+1][center.y-1];
				tempNode->neigh.push_back(btmLeft);
			}
			if(xMore){//btmMid
				Node* btmMid = matrix[center.x+1][center.y];
				tempNode->neigh.push_back(btmMid);
			}
			if(xMore && yMore){//btmRight
				Node* btmRight = matrix[center.x+1][center.y+1];
				tempNode->neigh.push_back(btmRight);
			}

			//std::cout<<"tempNode:"<<tempNode->xPos<<","<<tempNode->yPos<<std::endl;
			//tempNode->printNeigh();

		}
	}

}//initMatrix

void weightNode(Node* nodePassed, coord goal){//wieghts each node's neighbors
	//std::cout<<"nodeX:"<<nodePassed->xPos<<" nodeY:"<<nodePassed->yPos;
	int parentDistX = std::abs(nodePassed->xPos - goal.x);//parent x dist to node
	int parentDistY = std::abs(nodePassed->yPos - goal.y);//parent y dist to node
	//std::cout<<"parentDisX"<<parentDistX<<" parentDistY"<<parentDistY<<std::endl;
	int tempDistX = 0;
	int tempDistY = 0;
	for (auto x : nodePassed->neigh){//for each neighbor of nodePassed
		
		tempDistX = std::abs(x->xPos - goal.x);
		tempDistY = std::abs(x->yPos - goal.y);

		if((tempDistX <= parentDistX) && (tempDistY <= parentDistY)){//x and y closer
			//std::cout<<"x and y closer"<<std::endl;
			nodePassed->neighWeight.push_back(1);
		}
		else if((tempDistX <= parentDistX) || (tempDistY <= parentDistY)){//x or y closer
			//std::cout<<"x or y closer"<<std::endl;
			nodePassed->neighWeight.push_back(5);
		}
		else{//x and y not closer
			//std::cout<<"x and y not closer"<<std::endl;
			nodePassed->neighWeight.push_back(10);
		}
	}
}//weightNode


void dij(coord start, coord goal){
    Node* currNode = matrix[start.x][start.y];
	std::cout<<start.x<<","<<start.y<<";"<<currNode->xPos<<","<<currNode->yPos<<std::endl;
	currNode->weight = 0;
	int weightNew;
	int weightOld;
	priority_queue<Node*,  vector<Node*>, compareTo> queueNodes;
	do{
		currNode->visited = true;
		weightNode(currNode, goal);

		int i=0;
		for (auto x : currNode->neigh){//for each neighbor of nodePassed
			
			weightNew = currNode->neighWeight.at(i) + currNode->weight;
			weightOld = x->weight;

			//std::cout<<"weightNew:"<<weightNew<<", weightOld:"<<weightOld<<std::endl;

			if(weightNew < weightOld){
				//std::cout<<"new < old"<<std::endl;
				x->weight = weightNew;
				x->parent = currNode;
				coord tempCoord;
				tempCoord.x = x->xPos;
				tempCoord.y = x->yPos;
				if(canPass(tempCoord) && (x->visited==false)){
					//std::cout<<"pushing on queue"<<std::endl;
					queueNodes.push(x);
				}
			}
			//std::cout<<"i:"<<i<<std::endl;
			i++;
		}

		currNode = queueNodes.top();
		queueNodes.pop();
		if(currNode==matrix[goal.x][goal.y]){
			break;
		}
	}while(!queueNodes.empty());

	int costTime = 0;
	while(currNode->parent != NULL){
		image.draw_line(currNode->xPos, currNode->yPos, currNode->parent->xPos, currNode->parent->yPos, redColor);
		image.display(mainDisplay);
		currNode = currNode->parent;
		costTime++;
	}
	cout<<"Cost:"<<costTime<<endl;
}//end dij

int main(){
	w = image.width();//get pixel width from image
	//std::cout<<w<<std::endl;
	h = image.height();//get pixel height from image
	//std::cout<<h<<std::endl;
	const unsigned char red[] = { 255,0,0 }, green[] = { 0,255,0 }, blue[] = { 0,0,255 };//for rgb detection
	coord start, goal;
	bool toGoal = false;
	while(!mainDisplay.is_closed()){
		
		mainDisplay.wait();
		if (mainDisplay.button() && mainDisplay.mouse_y()>=0){//for mouse click position
      		int x = mainDisplay.mouse_x();
			int y = mainDisplay.mouse_y();
			std::cout<<"x: "<<x<<std::endl;
			std::cout<<"y: "<<y<<std::endl;
			
			if(!toGoal){
				start.x = x;
				start.y = y;
				toGoal = true;
			}
			else{
				goal.x = x;
				goal.y = y;
				
				high_resolution_clock::time_point t1 = high_resolution_clock::now(); 
				initMatrix();
				dij(start, goal);
				high_resolution_clock::time_point t2 = high_resolution_clock::now();
				duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
				std::cout<<"duration (in seconds): "<<time_span.count()<<std::endl;
				toGoal = false;
			}			
			
		}
	}
}//main