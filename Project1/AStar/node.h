#ifndef NODE_H
#define NODE_H
#include <vector>
#include <iostream>
using namespace std;

class Node
{
    public:
        Node();
        Node* parent;
        int weight;
        bool canUse;
        bool visited;
        vector<Node*> neigh;
        vector<int> neighWeight;
        int xPos;
        int yPos;
        void printNeigh();
    private:
          
};

#endif //GAME_H