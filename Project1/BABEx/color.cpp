#include "color.h"

Color::Color(){
    red = 0;
    green = 0;
    blue = 0;
}//default constructor

Color::Color(int red, int green, int blue){
    this->red = red;
    this->green = green;
    this->blue = blue;
    
}//overloaded constructor

int Color::getRed(){
    return red;
}//get red

int Color::getGreen(){
    return green;
}//get green

int Color::getBlue(){
    return blue;
}//get blue