#include "node.h"
#include <bits/stdc++.h>
#include <stdio.h> 
#include <limits.h>
using namespace std;
int V; 

void printPath(int parent[], int find, int dist[]){
    int j =0;
    for(int i=0;i<V;i++)
		if(i!=find)
		{
			cout<<"\nDistance of node "<<i<<" = "<<dist[i];
			cout<<"\nPath ="<<i;
			
			j=i;
			do
			{
				j=parent[j];
				cout<<"<-"<<j;
			}while(j!=find);
	}
    
}//end printDist

int minDist(int dist[], bool vertMark[]){
    int min = INT_MAX;//set highest possible min to start
    int min_index;//vertext with shortest path not yet in the path

    for(int i=0; i< V; i++){//go through each vertex
        if(vertMark[i] == false && dist[i] <= min){ //if vertex not yet visited, and lower than local min
            min_index = i; //store index
            min = dist[i]; //save new local min
        }
    }

    return min_index;
}//end minDist

void dij(vector<int> adj[], int find){
    int dist[V]; //path
    bool vertMark[V]; //true if vert is part of shortest path

    int parent[V];
    //initialize all to max and not visited
    for(int i=0; i<V; i++){
        dist[i] = INT_MAX; //max out each path for comparisons
        vertMark[i] = false; //no paths included so far
    }

    //actual location of goal
    dist[find] = 0;
    parent[find]= -1;
   
    vector<int> nodesTaken[V]; //map each of the nodes taken
    //shortest path for each vert.
    for(int count=0; count<V; count++){
        int u = minDist(dist, vertMark);
        //cout<<"u:"<<u<<endl;
        vertMark[u] = true;

        for(int v=0; v<V; v++){
            if (adj[u][v] && vertMark[v] == false && adj[u][v] < dist[v]){
                dist[v] = dist[u] + adj[u][v];
                //cout<<"count:"<<count <<" U:"<<u<<" V:"<<v<<" dist[v]:"<<dist[v]<<" dist[u]:"<<dist[u]<<" adj[u][v]:"<<adj[u][v]<<endl;
                parent[v] = u;//add that node to the path
            }
        }
         
    }
 
    printPath(parent, find, dist);
}//end dij

//initialize all values to zero
void initVector(vector<int> adj[]){
    for(int i=0; i<V; i++){
        for(int j=0; j<V; j++){
            adj[i].push_back(0);
        }
    }

}

//print matrix for debugging
void printMatrix(vector<int> adj[]){
    for(int i=0; i<V; i++){
        for(int j=0; j<V; j++){
            cout<<adj[i][j]<<" ";
        }
        cout<<endl;
    }
}

void addEdge(vector<int> adj[], int node1, int node2, int weight) 
{ 
    adj[node1][node2] = weight; //weight represented in 2D array (using vectors)
    adj[node2][node1] = weight; //for undirected part need to do inverse
} 

int main()//NOTE: everything starts at zero, so nodes start at zero and go one less than # of verts.
{
    cout<<"Enter the number of verticies: ";
    cin>>V; //global var declaration of # of vert.
    vector<int> adj[V];
    initVector(adj); //must be initialized to allocate space (NOTE: if have time - find another way to do this)
    printMatrix(adj);
    int node1, node2, weight;
    do{
        cout<<"Enter inputs in the following format, or 0 0 0 to exit: (node1, node2, distance between):";
        cin >> node1 >> node2 >> weight;
        addEdge(adj, node1, node2, weight);
        printMatrix(adj);
    }while(!(node1==0 && node2==0 && weight==0));

    int startingNode;
    cout<<"Enter the starting node: ";
    cin>>startingNode;

    dij(adj, startingNode);
    //cout<<INT_MAX; //getting max int for earlier path
    return 0;
}