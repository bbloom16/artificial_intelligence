#include "node.h"

Node::Node(){
    weight = 0;
    node1 = 0;
    node2 = 0;
}//default constructor

Node::Node(int node1, int node2, int weight){
    this->node1 = node1;
    this->node2 = node2;
    this->weight = weight;
    
}//overloaded constructor

int Node::getWeight(){
    return weight;
}//get weight

int Node::getNode1(){
    return node1;
}//get node1

int Node::getNode2(){
    return node2;
}//get node2