#ifndef NODE_H
#define NODE_H

class Node
{
    public:
        Node();
        Node(int node1, int node2, int weight);
        int getNode1();
        int getNode2();
        int getWeight();
        
    private:
        int node1;
        int node2;
        int weight;
       
};

#endif //GAME_H