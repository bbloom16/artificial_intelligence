import java.util.*;

public class Neuron{

    public double[] x;
    public double[] w;
    public int y;

    public Neuron(double[] x, int y){
        this.x = x;
        this.y = y;

        //init all weights to random val between 0-1
        this.w = new double[NeuralNetwork.NUM_INPUTS*NeuralNetwork.NUM_HIDDEN];
        for(int i=0; i<NeuralNetwork.NUM_INPUTS*NeuralNetwork.NUM_HIDDEN; i++){
            this.w[i] = Math.random();//0-1
        }

    }//end constructor

    public String toString(){
        String listX = "";
        for(int i=0; i<x.length; i++){
            listX += "x" + i + ": " + x[i] + ", ";
        }
        return listX + " y:" + y;
    }//end toString

}