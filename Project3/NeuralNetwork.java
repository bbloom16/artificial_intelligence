import java.util.*;
import java.io.*;
import java.lang.Math;

public class NeuralNetwork{
    
    static final int NUM_LAYERS = 3;//3 layers (L1: Input, L2: Hidden, L3: Output)
    static final int NUM_INPUTS = 4;//4 inputs (x1,x2,x3,x4)
    static final int NUM_OUTPUTS = 1;//1 output of three choices L3 (0: Iris-setosa, 1: Iris-versicolor, 2: Iris-virginica)
    static final int NUM_HIDDEN = 5;//5 hidden Neurons (L2)
    static final int INPUT_FILE_LINES = 150;//150 lines in file
    static final String INPUT_FILE_NAME = "iris.data";//name of input file

    public static void activationFunction(Neuron[] listPassed){
        
        double[] hidden = new double[NUM_HIDDEN];
        double[] outputWeights = new double[NUM_HIDDEN];
        double[] inputWeights = new double[NUM_HIDDEN*NUM_INPUTS];

        for(int i=0; i<listPassed.length; i++){
            Neuron currNeuron = listPassed[i];

            
            if(i==0){//first run
                int inputWeightCount = 0;
                for(int h=0; h<NUM_HIDDEN; h++){//for each hidden node
                    //neuron bias
                    double b = 1.0;//set bias to +1
                    //Math.random()*2 - 1;//number btwn -1 and 1
                    //System.out.println("b: " + b);
                    
                    //pre-activation
                    double activationTotal = 0;
                    for(int j=0; j<NUM_INPUTS; j++){
                        //init input weights
                        inputWeights[inputWeightCount] = currNeuron.w[j];
                        inputWeightCount++;
                        
                        activationTotal += currNeuron.x[j]*currNeuron.w[h+(j*NUM_HIDDEN)];
                    }
                    hidden[h] = Math.tanh(activationTotal + b);
                    //hidden[h] = Math.tanh((currNeuron.x[0]*currNeuron.w[h]) + (currNeuron.x[1]*currNeuron.w[h+5]) + (currNeuron.x[2]*currNeuron.w[h+10]) + (currNeuron.x[3]*currNeuron.w[h+15]) + b);
                    System.out.println("hidden [: " + h + "]:" + hidden[h]);
                }
                for(int j=0; j<NUM_HIDDEN; j++){
                    outputWeights[j] = Math.random();
                }
            }//end first run

            double outSums = 0;
            for(int j = 0; j < NUM_HIDDEN; j++){
                outSums += hidden[j] * outputWeights[j];
            }

            double output = Math.tanh(outSums);
            System.out.println("first output: " + output);
            //deravative:
            double deltaOfSum =  1- (Math.pow(output, 2));
            System.out.println("deltaOfSum: " + deltaOfSum);

            //calculate delta output weights
            double[] deltaOutputWeights = new double[NUM_HIDDEN];

            for(int j=0; j<NUM_HIDDEN; j++){
                deltaOutputWeights[j] = deltaOfSum / hidden[j];
            }

            //calculate new output weights
            double[] newOutputWeights = new double[NUM_HIDDEN];
            
            for(int j=0; j<NUM_HIDDEN; j++){
                newOutputWeights[j] = hidden[j] + deltaOutputWeights[j];
            }

            //calculate delta input sum
            double[] deltaInputHiddenSum = new double[NUM_HIDDEN];
            
            for(int j=0; j<NUM_HIDDEN; j++){
                deltaInputHiddenSum[j] = (deltaOfSum/deltaOutputWeights[j]) * Math.tanh(hidden[j]);
            }
            
            //calculate delta input hidden weights
            double[] deltaInputHiddenWeights = new double[NUM_HIDDEN*NUM_INPUTS];

            int k = 0;
            for(int j=0; j<NUM_INPUTS; j++){
                for(int l=0; l<NUM_HIDDEN; l++){
                    if(currNeuron.x[j]==0){
                        deltaInputHiddenWeights[k] = 0;//if input is 0, return 0 to avoid divide by zero
                    }
                    else{
                        deltaInputHiddenWeights[k] = deltaInputHiddenSum[l] / currNeuron.x[j];
                    }
                    k++;
                }
            }

            //revise input weights factoring in deltaInputHiddenWeights
            for(int j=0; j<NUM_INPUTS*NUM_HIDDEN; j++){
                inputWeights[j] = inputWeights[j] + deltaInputHiddenWeights[j];
            }

            //swap newOutputWeights to outputWeights
            for(int j=0; j<NUM_HIDDEN; j++){
                outputWeights[j] = newOutputWeights[j];
            }

            //final output
            double tempFinalOutput = 0;
            for(int j=0; j<NUM_HIDDEN; j++){
                tempFinalOutput += hidden[j]*outputWeights[j];
            }

            output = Math.tanh(tempFinalOutput);
            System.out.println("revised output: " + output);

            System.out.println();
        }
        
    }//end activationFunction

    public static Neuron[] ReadFile(String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        Scanner in = new Scanner(file);

        int i=0;
        Neuron[] objList = new Neuron[INPUT_FILE_LINES];
        while(in.hasNextLine()){
        
            String input = in.nextLine();
            String[] values = input.split(",");

            if(values.length==NUM_INPUTS+NUM_OUTPUTS){
                double[] x = new double[NUM_INPUTS];
                int y;
                for(int j=0; j<x.length; j++){
                    x[j] = Double.parseDouble(values[j]);
                    //System.out.println(x[j]);
                }
                
                String tempY = values[NUM_INPUTS];
                if(tempY.equals("Iris-virginica")){
                    y = 2;
                }
                else if(tempY.equals("Iris-versicolor")){
                    y = 1;
                }
                else{
                    y = 0;
                }
                //System.out.println("y: " + y);

                Neuron thisLine = new Neuron(x,y);
                //System.out.println(thisLine.toString());//for de-bugging
                objList[i] = thisLine;
                i++;
            }
        }

        return objList;
    }

    public static void main(String [] args) throws FileNotFoundException {
        Neuron[] listPassed = ReadFile(INPUT_FILE_NAME);
        activationFunction(listPassed);
    }
}