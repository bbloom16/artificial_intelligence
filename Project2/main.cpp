/*
 * Christian Esteves
 * Basic Genetic Algorithm Example
 * Fall 2016
 * COMP 399 - Hands on Computing
 *
 * This program will take all numbers between MIN_DATA_NUM and MAX_DATA_NUM (defined in gasettings.h) and
 * produce a properly sized array with each of the numbers appearing exactly once.
 */

#include "geneticAlgorithm.h"
#include <time.h>

#include "infastructure.h"
Infastructure *structSet = new Infastructure();;

int main()
{
    //Seed the random to the current system time
    srand(time(NULL));

    
    
    //NOTE: number of relay devices can be set by changing CHROMOSOME_SIZE in gasettings.h
    std::cout<<"Enter the bandwidth deduction for each relay device (format xx with be interpreted as 0.xx): "<<std::endl;
    for(int i=0; i<CHROMOSOME_SIZE; i++){
        int val;
        std::cout<<"Relay Point "<<i<<": ";
        std::cin>>val;
        structSet->setDeduction(i, val);
    }

    std::cout<<"Enter the number of devices each relay device can service: "<<std::endl;
    for(int i=0; i<CHROMOSOME_SIZE; i++){
        int val;
        std::cout<<"Relay Point "<<i<<": ";
        std::cin>>val;
        structSet->setService(i, val);
    }
    
    
    //Create a new GeneticAlgorithm object
    GeneticAlgorithm ga;

    //Create a new Population object
    Population *pop = new Population(true);

    //Create a new chromosome to store our solution, pass in a new population
    Chromosome *solution = ga.Run(pop);

    //Output the solution to the user
    std::cout<<"Solution: ";
    solution->PrintChromosome();
    std::cout<<std::endl;
}
