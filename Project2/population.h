/*
 * Christian Esteves
 * Basic Genetic Algorithm Example
 * Fall 2016
 * COMP 399 - Hands on Computing
 */

#ifndef GENETICALGORITHM_H
#define GENETICALGORITHM_H

#include "gasettings.h"
#include "chromosome.h"
#include "infastructure.h"
#include <iomanip>

class Population
{

public:
    Population();
    //Constructors
    Population(bool initialize);

    //Getters & Setters
    Chromosome* GetChromosome(int i);
    void AddChromosome(int i, Chromosome *c);

    //Utility Functions
    void SortChromosomes();
    void PrintChromosomes();

    //GA Functions
    void MutatePopulation();
    void CalculatePopulationFitness();
    bool ConvergenceCheck();
private:
    //Each Population is an array of chromosomes
    Chromosome **chromosome_arr_;
};

#endif // GENETICALGORITHM_H
