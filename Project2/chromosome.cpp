/*
 * Christian Esteves
 * Basic Genetic Algorithm Example
 * Fall 2016
 * COMP 399 - Hands on Computing
 */

#include "chromosome.h"

#include "infastructure.h" 
extern Infastructure *structSet;

//Default constructor for our Chromosome, initializes a Gene array and calculates it's fitness
Chromosome::Chromosome()
{
    for(int i = 0; i < CHROMOSOME_SIZE; i++){
        gene_array_[i] = 0;
    }
    fitness_ = 0;
}

//Returns this chromosome's fitness
int Chromosome::GetFitness(){
    return fitness_;
}

//Retrieves a specific gene's data
int Chromosome::GetGeneData(int index){
    return gene_array_[index];
}

//Sets a specific gene's data to what is given
void Chromosome::SetGeneData(int index, int data){
    gene_array_[index] = data;
}

//Initializes this chromosome - sets all genes to a random number and calculate it's fitness
void Chromosome::Initialize(){
    for(int i = 0; i < CHROMOSOME_SIZE; i++){
        gene_array_[i] = (rand() % MAX_DATA_NUM) + MIN_DATA_NUM;
    }

    CalculateFitness();
}

//Prints this chromosome's genes
void Chromosome::PrintChromosome(){
    for(int i = 0; i < CHROMOSOME_SIZE; i++){
        std::cout<<gene_array_[i]<<" ";
    }
    std::cout<<std::endl;
}

//Each gene in this chromosome has a MUTATION_RATE chance of becoming a random number
void Chromosome::Mutate(){

    for(int i = 0; i < CHROMOSOME_SIZE; i++){
        if(((double) rand() / (RAND_MAX)) < MUTATION_RATE){
            gene_array_[i] = (rand() % MAX_DATA_NUM) + MIN_DATA_NUM;
        }
    }
}

//Calculates this chromosome's fitness based on our criteria
//The goal of this particular GA is to create an array of numbers between MIN_DATA_NUM and MAX_DATA_NUM, with each number appearing once
void Chromosome::CalculateFitness(){
    //std::cout<<"-------------"<<structSet->getDeduction(0)<<"----------"<<std::endl;
    //Used to calculate how many numbers there are 1 of
    int fitness = 0;
    //Used to check how many times a specific number appears
    int temp, deduction, service;

    //Loop through the chromosome
    for(int i = 0; i < CHROMOSOME_SIZE; i++){
        temp = gene_array_[i];
        //std::cout<<"temp:"<<temp<<std::endl;
        deduction = structSet->getDeduction(temp);
        service = structSet->getService(temp);
        
        //(incoming bandwidth set to 1, subtraced from bandwidth deduction)*by number of ports on relay device
        fitness = (100-deduction)*service;
        //std::cout<<"fitness:"<<fitness<<std::endl;
    }
    //Set this chromosome's fitness to the calculated sum
    fitness_ = fitness;
}
