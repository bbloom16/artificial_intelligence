/*
 * Christian Esteves
 * Basic Genetic Algorithm Example
 * Fall 2016
 * COMP 399 - Hands on Computing
 */

#include "population.h"

//The default constructor

Population::Population(){

}

Population::Population(bool initialize)
{
    //Set aside enough room in memory for all of our chromosome pointers
    chromosome_arr_ = new Chromosome*[POPULATION_SIZE];

    //If this is the population we're starting with
    if(initialize){
        //Loop through our array of pointers and create an object for each pointer to point to, then initialize it
        for(int i = 0; i < POPULATION_SIZE; i++){
            chromosome_arr_[i] = new Chromosome();
            chromosome_arr_[i]->Initialize();
        }
    }
}

//Retrieve a specific chromosome from the population
Chromosome* Population::GetChromosome(int i){
    return chromosome_arr_[i];
}

//Add a chromosome to the population at index i
void Population::AddChromosome(int i, Chromosome *c){
    chromosome_arr_[i] = c;
}

//Sorts the population by fitness from high to low
//Uses insertion sort. Since the number of chromosomes per generation is small, we need not worry much about efficiency.
void Population::SortChromosomes(){
    Chromosome *x;
    int j;
    for(int i = 1; i < POPULATION_SIZE; i++){
        x = chromosome_arr_[i];
        j = i - 1;
        while (j >= 0 && chromosome_arr_[j]->GetFitness() < x->GetFitness()){
            chromosome_arr_[j+1] = chromosome_arr_[j];
            j = j - 1;
        }
        chromosome_arr_[j+1] = x;
    }
    x = nullptr;
}

//Output the population
void Population::PrintChromosomes(){
    for(int i = 0; i < POPULATION_SIZE; i++){
        std::cout<<"Chromosome "<<i+1<<" - Fitness: "<<chromosome_arr_[i]->GetFitness()<<" - ";
        chromosome_arr_[i]->PrintChromosome();
    }
    std::cout<<std::endl;
}

//Loop through each Chromosome, mutate it, then re-calculate its fitness
void Population::MutatePopulation(){
    //If ELITISM is on, we need the most fit chromosome in the first index of the array
    //Cheaper than performing an insertion sort before AND after the mutate
    if(ELITISM){
        Chromosome *temp = chromosome_arr_[0];
        int bestIndex = 0;

        //Find the index and get the pointer to the most fit chromosome
        for(int i = 1; i < POPULATION_SIZE; i++){
            if(chromosome_arr_[i]->GetFitness() > temp->GetFitness()){
                temp = chromosome_arr_[i];
                bestIndex = i;
            }
        }

        //Swap the most fit chromosome and whatever is in position 0
        chromosome_arr_[bestIndex] = chromosome_arr_[0];
        chromosome_arr_[0] = temp;
        temp = nullptr;
    }

    //Mutate the children, saving the best Chromosome if elitism is on
    for(int i = (ELITISM ? 1:0); i < POPULATION_SIZE; i++){
        chromosome_arr_[i]->Mutate();
        chromosome_arr_[i]->CalculateFitness();
    }
}

//Loop through the population and calculate each chromosome's fitness
void Population::CalculatePopulationFitness(){
    for(int i = 0; i < POPULATION_SIZE; i++){
        chromosome_arr_[i]->CalculateFitness();
    }
}

//Check to see if the best chromosome in the population has reached convergence
bool Population::ConvergenceCheck(){

    if(chromosome_arr_[0]->GetFitness() == EXPECTED_FITNESS){
        return true;
    }
    return false;
}
