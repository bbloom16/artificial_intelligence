/*
 * Christian Esteves
 * Basic Genetic Algorithm Example
 * Fall 2016
 * COMP 399 - Hands on Computing
 */

#ifndef GASETTINGS_H
#define GASETTINGS_H

    //The smallest number we want in our array
    const int MIN_DATA_NUM = 1;
    //The largest number we want in our array
    const int MAX_DATA_NUM = 10;
    //The number of genes in each chromosome can be calculated as such
    const int CHROMOSOME_SIZE = (MAX_DATA_NUM - MIN_DATA_NUM) + 1;
    //The size of our population
    const int POPULATION_SIZE = 50;
    //The chance each gene has of mutating after every generation (usually between .1 and .2)
    const double MUTATION_RATE = 0.15;
    //The fitness we are aiming for, the win state
    const int EXPECTED_FITNESS = CHROMOSOME_SIZE;
    //If this is true, then the highest scoring chromosome does NOT undergo mutation
    const bool ELITISM = true;
    

#endif // GASETTINGS_H
