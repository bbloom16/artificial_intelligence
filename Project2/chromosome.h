/*
 * Christian Esteves
 * Basic Genetic Algorithm Example
 * Fall 2016
 * COMP 399 - Hands on Computing
 */

#ifndef CHROMOSOME_H
#define CHROMOSOME_H

#include "gasettings.h"
#include <iostream>

class Chromosome
{
public:
    //Constructors
    Chromosome();

    //Getters & Setters
    int GetFitness();
    int GetGeneData(int index);
    void SetGeneData(int index, int data);

    //Utility Functions
    void PrintChromosome();
    void Initialize();

    //GA Functions
    void Mutate();
    void CalculateFitness();
private:
    //Each chromosome has an array of genes and a fitness
    int gene_array_[CHROMOSOME_SIZE];
    int fitness_;

};

#endif // CHROMOSOME_H
