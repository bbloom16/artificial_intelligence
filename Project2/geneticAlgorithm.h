/*
 * Christian Esteves
 * Basic Genetic Algorithm Example
 * Fall 2016
 * COMP 399 - Hands on Computing
 */

#ifndef GAALGORITHM_H
#define GAALGORITHM_H

#include <stdlib.h>
#include "population.h"

class GeneticAlgorithm
{
public:
    //Constructors
    GeneticAlgorithm();

    //GA Functions
    Chromosome* Run(Population *pop);
    Population *EvolvePopulation(Population *pop);
    Chromosome *UniformCrossover(Chromosome *parent1, Chromosome *parent2);
private:
    //Each GA needs to keep track of the number of generations it went through
    int generation_ = 0;
};

#endif // GAALGORITHM_H
