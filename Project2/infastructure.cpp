#include "infastructure.h"

//The default constructor

Infastructure::Infastructure(){

}

int Infastructure::getDeduction(int index){
    return relayDeduction[index];
}

int Infastructure::getService(int index){
    return relayService[index];
}

void Infastructure::setDeduction(int index, int val){
    relayDeduction[index] = val;
}

void Infastructure::setService(int index, int val){
    relayService[index] = val;
}