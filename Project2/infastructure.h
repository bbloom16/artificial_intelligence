#ifndef INFASTRUCTURE_H
#define INFASTRUCTURE_H

#include "gasettings.h"
#include <iostream>

class Infastructure
{
public:
    //Constructors
    Infastructure();

    int getDeduction(int index);
    int getService(int index);
    void setDeduction(int index, int val);
    void setService(int index, int val);
    
private:
    int relayDeduction[MAX_DATA_NUM];
    int relayService[MAX_DATA_NUM];
};

#endif // INFASTRUCTURE_H