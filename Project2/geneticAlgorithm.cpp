/*
 * Christian Esteves
 * Basic Genetic Algorithm Example
 * Fall 2016
 * COMP 399 - Hands on Computing
 */

#include "geneticAlgorithm.h"

#define DEBUG true
#define STEP true
#define SHOW_POPULATION true

//Empty constructor
//You could put some of the code in main into this constructor, your call.
GeneticAlgorithm::GeneticAlgorithm()
{

}

//The main algorithm
Chromosome* GeneticAlgorithm::Run(Population *pop){
    //Loop until convergence has occured and we have a solution
    while(!pop->ConvergenceCheck()){

        //Increment the generation count
        generation_++;

        //Create a new population using the current population
        pop = EvolvePopulation(pop);

        //Mutate the population
        pop->MutatePopulation();

        //Sort the population's chromosomes - makes output more legible
        pop->SortChromosomes();

        //Show how the population evolves over time
        if(SHOW_POPULATION){
            std::cout<<"\t\tGeneration "<<generation_<<std::endl;
            pop->PrintChromosomes();
        }

        //Allows us to "pause" the GA between generations and determine how well it is performing
        if(STEP){
            char a;
            std::cin>>a;
        }
    }
    
    //If we broke out of the loop, we found a solution!
    std::cout<<"Solution found after "<<generation_<<" generations!"<<std::endl;
    
    //Returns the solution to the calling function - the best chromosome will always be stored in index 0 at this point
    return pop->GetChromosome(0);
}

//This function uses Fitness Proportionate Selection to select which chromosomes are used in crossover
Population *GeneticAlgorithm::EvolvePopulation(Population *pop){
    //The new population
    Population *newPop = new Population(false);

    //2D Chromosome pointer - used to store the information of the next child's parents
    Chromosome **parents = new Chromosome*[2];

    //Sort the population
    pop->SortChromosomes();

    if(DEBUG){
        pop->PrintChromosomes();
    }

    //Stores the index of the parents, used to debug
    int parent_indexes[2];

    //The following are used to calculate each chromosome's chance of reproducing
    double sum = 0;
    double random_double;
    double probability[POPULATION_SIZE];
    double last_probability = 0.0;

    //Loop through all the chromosomes from the current generation and sum their fitnesses
    for(int i = 0; i < POPULATION_SIZE; i++){
        sum += pop->GetChromosome(i)->GetFitness();
    }

    //Loop through each chromosome from this population, and calculate the probability of each of them being chosen based on their fitness score
    //Score this probability in the probability array
    for(int i = 0; i < POPULATION_SIZE ; i++){
        probability[i] = last_probability + ((double)pop->GetChromosome(i)->GetFitness() / sum);
        last_probability = probability[i];
    }

    //Loops the length of the Chromosome array and prints each Chromosome's fitness along with it's probabilistic range.
    if(DEBUG){
        for(int i = 0; i < POPULATION_SIZE ; i++){
            std::cout<<"Chromosome "<<i+1<<" fitness: "<<pop->GetChromosome(i)->GetFitness()<<" - "<<probability[i]<<"%"<<std::endl;
        }
    }

    //Now that each chromosome's probability of being selected has been calculated, loop through all of the empty spots in the population
    for(int i = 0; i < POPULATION_SIZE; i++){
        //We need 2 parents for each new chromosome
        for(int j = 0; j < 2; j++){
            //Pick a random number between 0 and 1
            random_double = ((double) rand() / (RAND_MAX));

            if(DEBUG){
                std::cout<<"Rand = "<<random_double<<std::endl;
            }

            //Go through all possible parents and see if they were selected
            for(int k = 0; k < POPULATION_SIZE; k++){

                if(DEBUG){
                    std::cout<<std::fixed<<"\tk = "<<k<<" - Fitness: "<<(double)pop->GetChromosome(k)->GetFitness()<<"\tProbability: "<<probability[k]<<std::endl;
                }
                //std::cout<<std::setprecision(4)<<std::fixed<<"\tk-1 = "<<k<<"\tProbability: "<<probability[k-1]<<std::endl;
                //If the random number fell within this chromosome's probability, set it as a parent
                if(random_double <= probability[k]){
                    //std::cout<<"Chromosome "<<k<<" chosen!"<<std::endl;
                    parent_indexes[j] = k;
                    parents[j] = pop->GetChromosome(k);
                    //There can only ever be ONE possibility. Stop checking once we've found it.
                    break;
                }

            }
        }

        //Print the 2 parents being used to calculate the new Chromosome
        if(DEBUG){
            std::cout<<"Parents: "<<parent_indexes[0]<<" and "<<parent_indexes[1]<<std::endl;
            std::cout<<"Parent 1: ";
            parents[0]->PrintChromosome();

            std::cout<<"Parent 2: ";
            parents[1]->PrintChromosome();
        }

        //Add this new chromosome to the population
        newPop->AddChromosome(i, UniformCrossover(parents[0], parents[1]));
    }

    //Delete all of the old chromosomes
    for(int i = 0; i < POPULATION_SIZE; i++){
        delete pop->GetChromosome(i);
    }
    //Delete the old population
    delete pop;

    //Returns the newly calculated population back to the Run() function
    return newPop;
}

//Accepts two chromosomes to use as parents and returns a child
Chromosome *GeneticAlgorithm::UniformCrossover(Chromosome *parent1, Chromosome *parent2){
    Chromosome *child = new Chromosome();
    double random_double;

    //Loop the length of the chromosome
    for(int i = 0; i < CHROMOSOME_SIZE; i++){
        //Select a random number between 0 and 1
        random_double = ((double) rand() / (RAND_MAX));

        //Each parent has a 50% chance of passing on it's gene in this position
        if(random_double < 0.5){
            child->SetGeneData(i, parent1->GetGeneData(i));
        }else{
            child->SetGeneData(i, parent2->GetGeneData(i));
        }
    }

    //Gives this child a fitness score
    child->CalculateFitness();

    if(DEBUG){
        std::cout<<"Child: ";
        child->PrintChromosome();
    }

    //Return this new Chromosome to be added to the population
    return child;
}
